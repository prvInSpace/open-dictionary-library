package cymru.prv.dictionary.common;

import cymru.prv.dictionary.common.json.JsonSerializable;
import org.json.JSONObject;

class UsageExample implements JsonSerializable {

    private final String exampleTargetLang;
    private final String exampleTranslation;
    private final String explanation;

    UsageExample(JSONObject obj){
        exampleTargetLang = obj.getString("text");
        exampleTranslation = obj.optString("trans", "");
        explanation = obj.optString("explanation", "");
    }

    public JSONObject toJson(){
        return new JSONObject()
                .put("text", exampleTargetLang)
                .put("trans", exampleTranslation)
                .put("explanation", explanation);
    }

}
