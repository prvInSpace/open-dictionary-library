package cymru.prv.dictionary.common;

/**
 * Used to when a reference is unable to be resolved
 *
 * @author Preben Vangberg
 * @since 1.2.0
 */
public class MissingReferenceException extends RuntimeException {

    MissingReferenceException(IDWordReference reference){
        super(String.format("%s references word %d, but no word exist with that ID",
                reference.origin.getNormalForm(),
                reference.target
        ));
    }

}
