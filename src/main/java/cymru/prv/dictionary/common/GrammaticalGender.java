package cymru.prv.dictionary.common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * An enum representing a lot of common
 * grammatical genders around the world
 * in different languages.
 *
 * Each grammatical gender is assigned a
 * one character code. This is to make it
 * simpler to access from JSON files
 *
 * For example:
 * m -> Masculine,
 * f -> Feminine,
 * mf -> Masculine & Feminine
 *
 * @author Preben Vangberg
 * @since 1.0.3
 */
public enum GrammaticalGender {
    MASCULINE('m'),
    FEMININE('f'),
    NEUTER('n'),
    ANIMATE('a'),
    INANIMATE('i'),
    COMMON('c');


    /**
     * Map to map single character codes to their
     * representative enum
     */
    private static final Map<Character, GrammaticalGender> oneLetterCodeToGenderMap = new HashMap<>();


    /*
     * Static initializer used to populate the single
     * character to enum map.
     */
    static {
        for(GrammaticalGender gender : values())
            oneLetterCodeToGenderMap.put(gender.oneLetterCode, gender);
    }

    private final char oneLetterCode;


    /**
     * Initiates a new enum. All grammatical genders
     * are assigned a single code so that they are
     * easier to access from the JSON files.
     *
     * @param oneLetterCode the one letter code of the gender
     */
    GrammaticalGender(char oneLetterCode) {
        this.oneLetterCode = oneLetterCode;
    }


    /**
     * The string form, i.e the form that
     * is exported through the API and similar
     * is the one letter code hence why we
     * return that.
     *
     * @return The one letter code  as a string.
     */
    @Override
    public String toString() {
        return "" + oneLetterCode;
    }


    /**
     * Parses the string and returns a list of
     * genders that are in the string.
     *
     * @return A list of grammatical genders in the string
     */
    public static Set<GrammaticalGender> fromString(String genderString){
        if(genderString == null)
            return null;
        Set<GrammaticalGender> genders = new HashSet<>();
        for(char c : genderString.toCharArray()){
            if(oneLetterCodeToGenderMap.containsKey(c))
                genders.add(oneLetterCodeToGenderMap.get(c));
            else
                throw new IllegalArgumentException(String.format("Unknown gender '%c'", c));
        }
        return genders;
    }

}
