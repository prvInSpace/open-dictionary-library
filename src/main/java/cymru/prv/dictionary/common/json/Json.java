package cymru.prv.dictionary.common.json;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * A class containing several helper functions
 * related to parsing of JSON that is used through
 * out the different components of the dictionary
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public final class Json {

    // To prevent initialisation
    private Json(){}


    /**
     * Returns the list of strings that in
     * the object with the key given if it
     * exists.
     * Otherwise it returns an empty list
     *
     * @param obj the object where the data is
     * @param key the key to search for
     * @return a list of strings
     */
    public static List<String> getStringList(JSONObject obj, String key){
        var list = getStringListOrNull(obj, key);
        if(list != null)
            return list;
        return new LinkedList<>();
    }


    /**
     * Tries to get a list of strings from the
     * object with the key if it is present.
     * Otherwise it will return null if
     * the key was found.
     *
     * @param obj the object where the data is
     * @param key the key to search for
     * @return a list of strings or null
     */
    public static List<String> getStringListOrNull(JSONObject obj, String key){
        if(obj.has(key)){
            List<String> list = new LinkedList<>();
            Object strings = obj.get(key);
            if(strings instanceof JSONArray)
                for(int i = 0; i < ((JSONArray) strings).length(); ++i)
                    list.add(((JSONArray) strings).getString(i));
            else
                list.add((String)strings);
            return list;
        }
        return null;
    }


    /**
     * If the key exist the function will return
     * the data of that field as a list of strings.
     * <p>
     * If not it will return whatever the default
     * supplier returns
     *
     * @param obj the object the data is located
     * @param key the key to search for
     * @param defaultFunc the default supplier
     * @return the data of the object if the key exist or default if not.
     */
    public static List<String> getStringListOrDefault(JSONObject obj, String key, Supplier<List<String>> defaultFunc){
        if(obj.has(key))
            return getStringList(obj, key);
        return defaultFunc.get();
    }


    /**
     * Converts a list of JsonSerializable objects
     * to a JSONArray with the objects.
     *
     * @param objects a list of JsonSerializable objects
     * @return a JSONArray with the converted objects
     */
    public static <T extends JsonSerializable> JSONArray toJsonArray(Iterable<T> objects){
        JSONArray array = new JSONArray();
        for(JsonSerializable object : objects)
            array.put(object.toJson());
        return array;
    }


    /**
     * Converts an JSONArray of objects into a list of objects
     * by calling the provided constructor.
     *
     * @param array the objects to convert
     * @param constructor a function to convert the JSONObjects to an object
     * @return list of the converted objects
     */
    public static <T> List<T> getObjectList(JSONArray array, Function<JSONObject, T> constructor){
        List<T> list = new LinkedList<>();
        for(int i = 0; i < array.length(); ++i)
            list.add(constructor.apply(array.getJSONObject(i)));
        return list;
    }

}
