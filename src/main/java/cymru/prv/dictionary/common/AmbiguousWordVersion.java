package cymru.prv.dictionary.common;

import org.json.JSONObject;


/**
 * A version of TranslationUnit that includes
 * some extra information about the specific
 * version. Is used by AmbiguousWord
 *
 * @author Preben Vangberg
 * @since 1.0.0
 * @see AmbiguousWord
 */
class AmbiguousWordVersion extends TranslationUnit {

    private final String version;

    AmbiguousWordVersion(String englishWord, String version){
        super(englishWord);
        this.version = version;
    }

    @Override
    void addWord(Word word) {
        super.addWord(word);
    }

    @Override
    JSONObject toJson(String originLangauge) {
        JSONObject obj = super.toJson(originLangauge);
        obj.getJSONArray("en")
                .getJSONObject(0)
                .put("version", version);
        return obj;
    }

    public String getText() {
        return version;
    }
}
