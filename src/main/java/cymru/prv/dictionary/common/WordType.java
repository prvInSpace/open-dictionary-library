package cymru.prv.dictionary.common;

/**
 * An enum representation of the type of a given word
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public enum WordType {
    verb, adjective, noun, adposition, conjunction, particle, adverb, pronoun
}
