package cymru.prv.dictionary.common;

import org.json.JSONObject;

/**
 * <p>
 * Represents a conjugation
 * </p>
 * <p>
 * This is simply an extensions of the inflect class.
 * A conjugation is just an inflection for a verb.
 * In order for the wording to be clearer when working
 * with verbs this class exists for that purpose.
 * </p>
 * @author Preben Vangberg
 * @since 1.0.0
 * @see Inflection
 */
public class Conjugation extends Inflection {

    /**
     * Creates a new inflection based on the data
     * in obj. For more information see the inflection class.
     *
     * @see Inflection
     * @param obj the data for the object
     */
    public Conjugation(JSONObject obj) {
        super(obj);
    }
}
