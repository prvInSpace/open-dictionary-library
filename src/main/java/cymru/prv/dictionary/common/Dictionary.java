package cymru.prv.dictionary.common;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Function;


/**
 * <p>
 * Represents a normal dictionary
 * </p>
 * <p>
 * Will automatically load any resources located in the resource folder
 * "/res/&lt;langcode&gt;"
 * </p>
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class Dictionary implements Iterable<Word> {

    private final String languageCode;

    private final Map<Long, Word> idWordMap = new HashMap<>();
    private final Map<WordType, Map<String, List<Word>>> wordLists = new HashMap<>();
    private final Map<WordType, Function<JSONObject, Word>> jsonObjectToWordMap;
    private final Map<String, List<Word>> lemmas = new HashMap<>();

    private final DictionaryList list;

    public Dictionary(){
        this(new DictionaryList(), "test");
    }

    public Dictionary(DictionaryList list, String languageCode){
        this(list, languageCode, Map.of());
        for(WordType type : WordType.values())
            wordLists.put(type, new HashMap<>());
    }

    public Dictionary(String languageCode, Map<WordType, Function<JSONObject, Word>> jsonObjectToWordMap) {
        this(new DictionaryList(), languageCode, jsonObjectToWordMap);
    }

    public Dictionary(DictionaryList list, String languageCode, Map<WordType, Function<JSONObject, Word>> jsonObjectToWordMap) {
        this.list = list;
        this.languageCode = languageCode;
        this.jsonObjectToWordMap = jsonObjectToWordMap;
        for(WordType type : jsonObjectToWordMap.keySet())
            wordLists.put(type, new HashMap<>());
        loadJsonFiles();
        list.addDictionary(this);
    }


    /**
     * Used to fetch translations for a given word
     * @param word The word to find the translation units for
     * @return a collection of translation units that matches the search
     */
    Collection<? extends TranslationUnit> getTranslationUnits(Word word) {
        return list.getTranslationUnits(word);
    }


    /**
     * Loops through all the word types
     * and read all the JSON files for that
     * type. Also runs the post initializers.
     */
    private void loadJsonFiles() {
        // Initialize words
        for (WordType type : jsonObjectToWordMap.keySet())
            getWordsFromInternalResourceFolder(type);

        // Verify references within the dictionary
        verifyReferences();
    }


    public void verifyReferences(){
        for (var list : wordLists.values())
            for(var wordList : list.values())
                for(var word : wordList)
                    word.verifyIDReferences();
    }


    /**
     * Loops through the files in the resource folder
     * for the given dictionary and type and adds
     * all the words to the wordList
     *
     * @param type     The word type of the words that are to be read
     * @throws RuntimeException if the files could not be read
     */
    private void getWordsFromInternalResourceFolder(WordType type) {
        String path = "/res/" + languageCode + "/" + type + "/";

        try {
            var files = ResourceReader.getFilesInDirectory(path);
            for (InputStream file : files) {
                addWords(type, new JSONArray(new String(file.readAllBytes())));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * Loops through a JSONArray of words and add them to the wordList in question.
     *
     * @param type     the word type of the objects in the array
     * @param array    the JSON array of word objects of the type
     */
    private void addWords(WordType type, JSONArray array) {
        for (Object o : array) {
            JSONObject obj = (JSONObject) o;
            Word word = jsonObjectToWordMap.get(type).apply(obj);
            addWord(word);
        }
    }


    /**
     * Adds the given word to the dictionary
     *
     * @param word the word to be added
     */
    void addWord(Word word){
        if(word.isInDictionary())
            throw new IllegalArgumentException("Word '" + word.getNormalForm() + "' is already in a dictionary");
        if (word.getId() >= 0) {
            if (idWordMap.containsKey(word.getId()))
                throw new RuntimeException("ID " + word.getId() + " already exist for word " + idWordMap.get(word.getId()));
            idWordMap.put(word.getId(), word);
        }

        var wordList = wordLists.get(word.getType());
        wordList.putIfAbsent(word.getNormalForm(), new LinkedList<>());
        wordList.get(word.getNormalForm()).add(word);

        for(String version : word.getRedirects()){
            wordList.putIfAbsent(version, new LinkedList<>());
            wordList.get(version).add(word);
        }

        List<String> forms = word.getVersions();
        for(String form : forms) {
            lemmas.putIfAbsent(form, new LinkedList<>());
            List<Word> words = lemmas.get(form);
            if(!words.contains(word))
                words.add(word);
        }
        word.addToDictionary(this);
        list.addWord(word);
    }


    /**
     * Returns a list of words that has the normalForm
     * given as a parameter. No words will be generated.
     *
     * @param normalForm The normalForm of the words
     * @return List of words with matching normal forms.
     */
    public List<Word> getWords(String normalForm) {
        List<Word> words = new LinkedList<>();
        for (Map<String, List<Word>> map : wordLists.values())
            if (map.containsKey(normalForm))
                words.addAll(map.get(normalForm));

        return words;
    }

    /**
     * Returns a list of words that has the normal
     * form as the given parameter and that is of
     * the correct type. If no words are found
     * then a word will be generated of that type
     *
     * @param normalForm The normal form of the words
     * @param type       The type of the words
     * @return a list of the words with the normal for of the correct type
     */
    public List<Word> getWords(String normalForm, WordType type) {
        if (!wordLists.containsKey(type))
            throw new RuntimeException("Dictionary " + languageCode + " does not contain an implementation for type " + type);

        // If the dictionary contains the word return the list
        if (wordLists.get(type).containsKey(normalForm))
            return new LinkedList<>(wordLists.get(type).get(normalForm));

        // Else generate a new word
        var obj = new JSONObject().put("normalForm", normalForm);
        return List.of(generateWord(obj, type));
    }

    /**
     * Generates a word with the given type using
     * the provided JSON object. This can be used
     * to test new configurations of words an
     * similar.
     *
     * @param data the JSON object of the word
     * @param type the type of the word
     * @return a generated word
     */
    public Word generateWord(JSONObject data, WordType type){
        if(!jsonObjectToWordMap.containsKey(type))
            return new Word(data, type);
        return jsonObjectToWordMap.get(type).apply(data)
                .addToDictionary(this)
                .setConfirmed(false);
    }


    /**
     * Returns the word with a given ID or null if the word
     * could not be found.
     *
     * @param id the id of a word
     * @return the word with that id or null if it does not exist
     */
    public Word getWordById(long id) {
        return idWordMap.getOrDefault(id, null);
    }


    /**
     * Returns the language code of the dictionary
     *
     * @return the language code of the dictionary
     */
    public String getLanguageCode() {
        return languageCode;
    }


    /**
     * Returns the number of dictionary entires.
     *
     * @return the number of dictionary entries
     */
    public long getNumberOfWords() {
        long n = 0;
        for (Map<String, List<Word>> wordList : wordLists.values())
            for (List<Word> words : wordList.values())
                n += words.size();
        return n;
    }


    /**
     * Returns a list of all the lemmas for any
     * given form.
     *
     * @param form The form to fetch the lemmas for.
     * @return a list of words that matches the lemma.
     */
    public List<Word> getLemmas(String form){
        return lemmas.getOrDefault(form, new LinkedList<>());
    }


    /**
     * A class used to iterate over the dictionary.
     * Iterates over the wordLists map, and keeps track
     * of the space within the map structure it is in.
     */
    private static class DictionaryIterator implements Iterator<Word> {

        private final Queue<Map<String, List<Word>>> wordTypeQueue = new LinkedBlockingQueue<>();
        private final Queue<List<Word>> normalFormQueue = new LinkedBlockingQueue<>();
        private final Queue<Word> wordQueue = new LinkedBlockingQueue<>();

        private Word next = null;

        private DictionaryIterator(Dictionary dictionary){
            this.wordTypeQueue.addAll(dictionary.wordLists.values());
            next();
        }

        private DictionaryIterator(Dictionary dictionary, WordType type){
            if(dictionary.wordLists.containsKey(type))
                this.wordTypeQueue.add(dictionary.wordLists.get(type));
            next();
        }

        @Override
        public boolean hasNext() {
            return next != null;
        }

        @Override
        public Word next() {
            while(normalFormQueue.isEmpty() && !wordTypeQueue.isEmpty())
                normalFormQueue.addAll(wordTypeQueue.poll().values());
            while(wordQueue.isEmpty() && !normalFormQueue.isEmpty())
                wordQueue.addAll(normalFormQueue.poll());
            var toReturn = next;
            next = wordQueue.poll();
            return toReturn;
        }
    }


    /**
     * Creates and returns an iterator that iterates over all the
     * words in the dictionary
     * @return An iterator that iterates over all the words in the dictionary
     */
    @NotNull
    @Override
    public Iterator<Word> iterator() {
        return new DictionaryIterator(this);
    }


    /**
     * Creates and returns an iterator that only iterates over the words of
     * the given type
     * @param type The word type to iterate over
     * @return An iterator for all the words of the given type in the dictionary
     */
    public Iterator<Word> iterateByType(WordType type){
        return new DictionaryIterator(this, type);
    }


    /**
     * Returns a list of all the words in the dictionary
     * @return A list of all the words in the dictionary
     */
    public List<Word> getAllWords(){
        List<Word> words = new LinkedList<>();
        for(Map<String, List<Word>> map : wordLists.values())
            for(List<Word> list : map.values())
                words.addAll(list);
        return words;
    }


    /**
     * Returns a list of all the words in the dictionary of the type provided
     * @param type Word type to fetch
     * @return A list of all words in the dictionary of the given type
     */
    public List<Word> getAllWordsByType(WordType type){
        if(!wordLists.containsKey(type))
            return new LinkedList<>();

        List<Word> words = new LinkedList<>();
        for(List<Word> list : wordLists.get(type).values())
            words.addAll(list);
        return words;
    }


    public String getVersion(){
        return "un-versioned";
    }


    /**
     * Creates a JSON object that contains a lot of meta information
     * about the dictionary
     * @return A JSON object with information about the dictionary
     */
    public JSONObject compileInformationToJson(){
        var numberByType = new JSONObject();
        for(WordType type : wordLists.keySet())
            numberByType.put(type.name(), wordLists.get(type).size());

        return new JSONObject()
                .put("lang", languageCode)
                .put("version", getVersion())
                .put("numberOfLemmas", getNumberOfWords())
                .put("wordsWithId", idWordMap.size())
                .put("numberOfForms", lemmas.size())
                .put("supportedTypes", new JSONArray(wordLists.keySet()))
                .put("numberOfWordsByType", numberByType);
    }

    /**
     * Exports the entire dictionary to a JSON object with all
     * words and word forms.
     * @return The entire dictionary as a JSON object
     */
    public JSONObject exportDictionaryToJson(){
        var words = new JSONArray();
        for(Word w : this)
            words.put(w.toJson());
        return compileInformationToJson()
                .put("words", words);
    }

}
