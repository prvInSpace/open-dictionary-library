package cymru.prv.dictionary.common;

import cymru.prv.dictionary.common.json.JsonSerializable;
import org.json.JSONObject;

/**
 * Represents a reference to a word.
 * For string literal references see LiteralWordReference.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 * @see Word
 * @see LiteralWordReference
 */
class IDWordReference implements JsonSerializable {

    final Word origin;
    final long target;

    IDWordReference(Word origin, long target){
        this.origin = origin;
        this.target = target;
    }

    Word resolveTarget() throws MissingReferenceException {
        if(origin.isInDictionary()) {
            var word = origin.getDictionary()
                    .getWordById(target);
            if (word != null)
                return word;
        }
        throw new MissingReferenceException(this);
    }

    @Override
    public JSONObject toJson() {
        var obj = new JSONObject()
                .put("id", target);
        if(origin.isInDictionary()){
            var dict = origin.getDictionary();
            obj.put("lang", dict.getLanguageCode());
            var word = dict.getWordById(target);
            if(word != null)
                obj.put("value", word.getNormalForm());
        }
        return obj;
    }
}
