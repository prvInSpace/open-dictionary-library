package cymru.prv.dictionary.common;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Map;

public class TestTranslations {

    private Dictionary getDict(){
        return getDict("test");
    }

    private Dictionary getDict(String name){
        return new Dictionary(name, Map.of(
                WordType.verb, (w) -> new Word(w, WordType.verb)
        ));
    }

    @Test
    public void testAmbiguousWords(){
        // Set error stream
        PrintStream originalErr = System.err;
        ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        System.setErr(new PrintStream(errContent));

        Dictionary dictionary = getDict("ambiguous");
        JSONObject obj = dictionary.getWords("testCold").get(0).toJson();
        JSONArray translations = obj.getJSONArray("translations");
        Assertions.assertEquals(2, translations.length());
        String expected = "Warning: Word cold contain ambiguous versions. Consider specifying which version is preferred.\n";
        Assertions.assertEquals(expected, errContent.toString());

        // set it back
        System.setErr(originalErr);
    }

    @Test
    public void testAmbiguousWordWithVersion(){
        Dictionary dict = getDict();
        JSONObject obj = dict.getWords("testCold").get(0).toJson();
        JSONArray translations = obj.getJSONArray("translations");
        Assertions.assertEquals(1, translations.length());
        JSONObject t = translations.getJSONObject(0).getJSONArray("en").getJSONObject(0);
        Assertions.assertEquals("disease", t.getString("version"));
        Assertions.assertEquals("en", t.getString("lang"));
    }

    @Test
    public void testShouldExportOtherLanguages(){
        DictionaryList list = new DictionaryList();

        // Set up the two dictionaries
        Dictionary dict1 = new Dictionary(list, "test", Map.of(
                WordType.verb, (w)-> new Word(w,WordType.verb)));
        Dictionary dict2 = new Dictionary(list, "bi", Map.of(
                WordType.verb, (w)-> new Word(w,WordType.verb)));

        JSONObject obj = dict1.getWords("testCold").get(0).toJson();
        JSONArray translations = obj.getJSONArray("translations");
        Assertions.assertEquals(1, translations.length());
        Assertions.assertEquals(2, translations.getJSONObject(0).length());
        JSONObject t = translations.getJSONObject(0).getJSONArray("bi").getJSONObject(0);
        Assertions.assertEquals("otherLanguage", t.getString("value"));
    }

    @Test
    public void testTranslationUnknownVersionShouldPrintError(){
        // Set error stream
        PrintStream originalErr = System.err;
        ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        System.setErr(new PrintStream(errContent));

        Dictionary dictionary = getDict("unknown");
        JSONObject obj = dictionary.getWords("test").get(0).toJson();
        JSONArray translations = obj.getJSONArray("translations");
        Assertions.assertEquals(0, translations.length());

        String expected = "Warning: Unknown version 999 for word cold\n";
        Assertions.assertEquals(expected, errContent.toString());

        // set it back
        System.setErr(originalErr);
    }

    @Test
    public void testTranslationWithStarShouldReturnAllVersions(){
        Dictionary dict = getDict();
        JSONObject obj = dict.getWords("testStar").get(0).toJson();
        JSONArray translations = obj.getJSONArray("translations");
        Assertions.assertEquals(2, translations.length());
    }

    @Test
    public void testDefaultNounTranslation(){
        // Set error stream
        PrintStream originalErr = System.err;
        ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        System.setErr(new PrintStream(errContent));

        Dictionary dictionary = new Dictionary("test", Map.of(
                WordType.noun, (w) -> new Word(w, WordType.noun)
        ));

        // Should only return 1 translation unit (i.e the default)
        JSONObject obj = dictionary.getWordById(1500).toJson();
        JSONArray translations = obj.getJSONArray("translations");
        Assertions.assertEquals(1, translations.length());

        // No error is expected
        String expected = "";
        Assertions.assertEquals(expected, errContent.toString());

        // set it back
        System.setErr(originalErr);
    }

    @Test
    public void testTranslationWithMissingAmbiguity(){
        PrintStream originalErr = System.err;
        ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        System.setErr(new PrintStream(errContent));

        DictionaryList list = new DictionaryList();
        Dictionary dict = new Dictionary(list,"test");
        Word word = new Word(new JSONObject()
                .put("normalForm", "word")
                .put("translations", "test_translation:1"),
                WordType.verb
        );
        dict.addWord(word);
        var words = list.getTranslationsByEnglishWord("test_translation");
        Assertions.assertEquals(1, words.size());
        Assertions.assertEquals("word",
                words.get(0).toJson()
                        .getJSONArray("test")
                        .getJSONObject(0)
                        .getString("value"));

        // Even though it redirects to the version without : it should still
        // throw a warning
        Assertions.assertEquals(
                "Warning: 'test_translation:1': No ambiguous unit for this word",
                errContent.toString().trim());
        System.setErr(originalErr);
    }

}
