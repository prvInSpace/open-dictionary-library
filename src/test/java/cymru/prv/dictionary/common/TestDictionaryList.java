package cymru.prv.dictionary.common;

import cymru.prv.dictionary.common.json.Json;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestDictionaryList {

    @Test
    public void testEmptyDictionaryList(){
        DictionaryList list = new DictionaryList();
        Assertions.assertFalse(list.hasDictionary("cy"));
        // Check if ambiguous words was loaded
        Assertions.assertTrue(list.getNumberOfAmbiguousWords() > 0);
    }

    @Test
    public void testShouldAddDictionaryToList(){
        DictionaryList list = new DictionaryList();
        var dict = new Dictionary(list, "cy");
        Assertions.assertTrue(list.hasDictionary("cy"));
        Assertions.assertEquals(dict, list.getDictionary("cy"));
    }

    @Test
    public void testGetEnglishTranslation(){
        DictionaryList list = new DictionaryList();
        Dictionary dict = new Dictionary(list, "cy");
        dict.addWord(new Word(new JSONObject()
                .put("normalForm", "testio")
                .put("translations", "to test"),
                WordType.verb
        ));
        var translations = Json.toJsonArray(list.getTranslationsByEnglishWord("to test"));
        var unit = translations.getJSONObject(0);
        var cy = unit.getJSONArray("cy");
        Assertions.assertEquals("testio", cy.getJSONObject(0).getString("value"));
    }

    @Test
    public void testMissingTranslation(){
        DictionaryList list = new DictionaryList();
        var translations = list.getTranslationsByEnglishWord("asdasdasdasdasd");
        Assertions.assertEquals(0, translations.size());
    }

    @Test
    public void testGetEnglishTranslationAmbiguous(){
        DictionaryList list = new DictionaryList();
        var translations = list.getTranslationsByEnglishWord("to know");
        for(int i = 0; i < 2; ++i){
            var trans = Json.toJsonArray(translations)
                    .getJSONObject(0)
                    .getJSONArray("en")
                    .getJSONObject(0);
            Assertions.assertNotNull(trans.optString("version", null));
        }
    }

    @Test
    public void testShouldGetAmbiguousWord(){
        DictionaryList list = new DictionaryList();
        var word = list.getAmbiguousWord("to know");
        Assertions.assertNotNull(word.getEnglishWord());
    }
}
