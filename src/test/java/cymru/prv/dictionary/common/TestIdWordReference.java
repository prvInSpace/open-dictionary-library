package cymru.prv.dictionary.common;

import cymru.prv.dictionary.common.json.JsonSerializable;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestIdWordReference {

    @Test
    public void testLiteralWordReferenceShouldExportCorrectly(){
        var dict = new Dictionary();
        Word word = new Word(
                new JSONObject().put("normalForm", "test").put("id", 5), WordType.noun);
        dict.addWord(word);
        JsonSerializable ref = new IDWordReference(word, word.getId());
        Assertions.assertEquals("test", ref.toJson().get("value"));
        Assertions.assertEquals(5, ref.toJson().getLong("id"));
    }

    @Test
    public void testIDWordReferenceShouldThrowExceptionIfUnableToResolveTarget(){
        var dict = new Dictionary();
        dict.addWord(new Word(new JSONObject()
                .put("normalForm", "test")
                .put("versions", List.of(3)),
                WordType.verb
        ));
        Assertions.assertThrows(
                MissingReferenceException.class,
                dict::verifyReferences,
                "test: references word 3, but no word exist with that ID"
        );
    }


}
