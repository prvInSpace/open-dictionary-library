package cymru.prv.dictionary.common;

import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

public class TestAmbiguousWord {

    @Test
    public void testShouldExportToJson(){
        JSONObject obj = new JSONObject();
        obj.put("word", "test");
        obj.put("versions", new JSONObject()
                .put("1", "testversion"));

        AmbiguousWord word = new AmbiguousWord(obj);
        JSONObject json = word.toJson();
        Assertions.assertEquals(json.getString("word"), "test");
        JSONObject versions = json.getJSONObject("versions");
        Assertions.assertEquals("testversion", versions.getString("1"));
    }

    @Test
    public void testShouldReturnAllUnits(){
        JSONObject obj = new JSONObject()
                .put("word", "test")
                .put("versions", new JSONObject()
                        .put("1", "version1")
                        .put("2", new JSONObject()
                                .put("text", "version2")));

        AmbiguousWord word = new AmbiguousWord(obj);
        List<AmbiguousWordVersion> json = word.getTranslationUnits();
        Assertions.assertIterableEquals(
                List.of("version1", "version2"),
                json.stream().map(AmbiguousWordVersion::getText).collect(Collectors.toList()));
    }

}
