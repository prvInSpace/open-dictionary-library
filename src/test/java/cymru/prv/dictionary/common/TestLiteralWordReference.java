package cymru.prv.dictionary.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestLiteralWordReference {

    @Test
    public void testLiteralWordReferenceShouldExportCorrectly(){
        LiteralWordReference ref = new LiteralWordReference("Hello, World!");
        Assertions.assertEquals("Hello, World!", ref.toJson().get("value"));
        Assertions.assertFalse(ref.toJson().has("id"));
    }

}
