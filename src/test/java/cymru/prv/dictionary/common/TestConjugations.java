package cymru.prv.dictionary.common;

import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConjugations {

    class VerbTest extends Word {

        private String stem;

        public VerbTest(String word, String stem){
            super(new JSONObject().put(NORMALFORM, word), WordType.verb);
            this.stem = stem;
        }

        @Override
        public JSONObject getConjugations() {
            return new JSONObject()
                    .put("testConjugation", stem + "-st");
        }
    }

    @Test
    public void testWordShouldExportConjugations(){
        VerbTest verb = new VerbTest("test", "te");
        JSONObject obj = verb.toJson();
        Assertions.assertEquals("te-st", obj.getJSONObject("conjugations").get("testConjugation"));
    }

}
