package cymru.prv.dictionary.common.conjugation;

import cymru.prv.dictionary.common.Inflection;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class WelshPastInflection extends Inflection {

    private String stem;

    public WelshPastInflection(JSONObject obj) {
        super(obj);
        stem = obj.getString("stem").replaceFirst("io$", "");
    }

    @Override
    public List<String> getDefaultSingularFirst() {
        return Collections.singletonList(stem + "ais");
    }

    @Override
    public List<String> getDefaultSingularSecond() {
        return Collections.singletonList(stem + "aist");
    }

    @Override
    public List<String> getDefaultSingularThird() {
        return Collections.singletonList(stem + "odd");
    }

    @Override
    public List<String> getDefaultPluralFirst() {
        return Collections.singletonList(stem + "on");
    }

    @Override
    public List<String> getDefaultPluralSecond() {
        return Collections.singletonList(stem + "och");
    }

    @Override
    public List<String> getDefaultPluralThird() {
        return Collections.singletonList(stem + "on");
    }
}
