package cymru.prv.dictionary.common.conjugation;

import cymru.prv.dictionary.common.Inflection;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

public class TestInflection {

    static class InflectionImp extends Inflection {
        protected String stem;

        public InflectionImp(JSONObject obj) {
            super(obj);
            this.stem = obj.getString("stem");
        }
    }

    static class SingFirstInflection extends InflectionImp {

        public SingFirstInflection(JSONObject obj) {
            super(obj);
        }

        @Override
        public List<String> getDefaultSingularFirst() {
            return Collections.singletonList(stem + "ais");
        }
    }

    @Test
    public void testConjugationImplementationEmpty(){
        JSONObject object = new InflectionImp(new JSONObject().put("stem", "hello")).toJson();
        Assertions.assertEquals(0, object.keySet().size());
    }

    @Test
    public void testSingularFirstConjugationWithoutOverride(){
        SingFirstInflection conj = new SingFirstInflection(new JSONObject().put("stem", "test"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testais"), obj.getJSONArray("singFirst"));
    }

    @Test
    public void testSingularFirstConjugationWithOverride(){
        SingFirstInflection conj = new SingFirstInflection(new JSONObject().put("stem", "test").put("singFirst", "testing"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testing"), obj.getJSONArray("singFirst"));
    }

    // Tests for Welsh Past Conjugation

    @Test
    public void testSingluarFirstWithStringWithoutOverride(){
        SingFirstInflection conj = new SingFirstInflection(
                new JSONObject().put("stem", "test"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testais"), obj.getJSONArray("singFirst"));
    }

    @Test
    public void testSingluarFirstWithArrayOverride(){
        SingFirstInflection conj = new SingFirstInflection(
                new JSONObject().put("stem", "test").put("singFirst", List.of("testing")));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testing"), obj.getJSONArray("singFirst"));
    }

    @Test
    public void testSingluarFirstWithStringOverride(){
        SingFirstInflection conj = new SingFirstInflection(
                new JSONObject().put("stem", "test").put("singFirst", "testing"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testing"), obj.getJSONArray("singFirst"));
    }

    @Test
    public void testSingluarSecondWithOverride(){
        WelshPastInflection conj = new WelshPastInflection(
                new JSONObject().put("stem", "testio").put("singSecond", "testing"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testing"), obj.getJSONArray("singSecond"));
    }

    @Test
    public void testSingluarSecondWithoutOverride(){
        WelshPastInflection conj = new WelshPastInflection(
                new JSONObject().put("stem", "testio"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testaist"), obj.getJSONArray("singSecond"));
    }

    @Test
    public void testSingluarThirdWithOverride(){
        WelshPastInflection conj = new WelshPastInflection(
                new JSONObject().put("stem", "testio").put("singThird", "testing"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testing"), obj.getJSONArray("singThird"));
    }

    @Test
    public void testSingluarThirdWithoutOverride(){
        WelshPastInflection conj = new WelshPastInflection(
                new JSONObject().put("stem", "testio"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testodd"), obj.getJSONArray("singThird"));
    }

    @Test
    public void testPluralFirstWithOverride(){
        WelshPastInflection conj = new WelshPastInflection(
                new JSONObject().put("stem", "testio").put("plurFirst", "testing"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testing"), obj.getJSONArray("plurFirst"));
    }

    @Test
    public void testPluralFirstWithoutOverride(){
        WelshPastInflection conj = new WelshPastInflection(
                new JSONObject().put("stem", "testio"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("teston"), obj.getJSONArray("plurFirst"));
    }

    @Test
    public void testPluralSecondWithOverride(){
        WelshPastInflection conj = new WelshPastInflection(
                new JSONObject().put("stem", "testio").put("plurSecond", "testing"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testing"), obj.getJSONArray("plurSecond"));
    }

    @Test
    public void testPluralSecondWithoutOverride(){
        WelshPastInflection conj = new WelshPastInflection(
                new JSONObject().put("stem", "testio"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testoch"), obj.getJSONArray("plurSecond"));
    }

    @Test
    public void testPluralThirdWithOverride(){
        WelshPastInflection conj = new WelshPastInflection(
                new JSONObject().put("stem", "testio").put("plurThird", "testing"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("testing"), obj.getJSONArray("plurThird"));
    }

    @Test
    public void testPluralThirdWithoutOverride(){
        WelshPastInflection conj = new WelshPastInflection(
                new JSONObject().put("stem", "testio"));
        JSONObject obj = conj.toJson();
        Assertions.assertIterableEquals(List.of("teston"), obj.getJSONArray("plurThird"));
    }

    @Test
    public void testJsonVariableHasShouldBeSet(){
        WelshPastInflection conj = new WelshPastInflection(
                new JSONObject().put("stem", "").put("has", false)
        );
        Assertions.assertFalse(conj.has());
    }

    @Test
    public void testEmptyInflectionShouldReturnNoVersions(){
        Inflection inflection = new Inflection(new JSONObject());
        Assertions.assertEquals(0, inflection.getVersions().size());
    }

    @Test
    public void testShouldReturnVersionIfPresent(){
        String singFirst = "singular first";
        Inflection inflection = new Inflection(new JSONObject()
            .put("singFirst", singFirst));
        Assertions.assertIterableEquals(List.of(singFirst), inflection.getVersions());
    }

    @Test
    public void testShouldReturnDefaultVersion(){
        Inflection inflection = new Inflection(new JSONObject()){
            @Override
            protected List<String> getDefaultSingularFirst() {
                return List.of("default");
            }
        };

        Assertions.assertIterableEquals(List.of("default"), inflection.getVersions());
    }

}
