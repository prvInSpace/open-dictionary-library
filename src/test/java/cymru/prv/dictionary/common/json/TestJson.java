package cymru.prv.dictionary.common.json;

import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

public class TestJson {

    @Test
    public void testToJsonArray(){
        List<Word> words = List.of(
                new Word(
                        new JSONObject().put("normalForm", "test"), WordType.noun)
        );

        JSONArray array = Json.toJsonArray(words);
        Assertions.assertEquals(1, array.length());
        Assertions.assertEquals("test", array.getJSONObject(0).getString("normalForm"));
    }

    @Test
    public void testObjectDoesNotHaveKey(){
        Assertions.assertIterableEquals(
                Collections.emptyList(),
                Json.getStringList(new JSONObject(), "hello"));
    }

    @Test
    public void testListOrDefaultWithKey(){
        var strings = List.of("string1", "string2");
        JSONObject obj = new JSONObject().put("strings", strings);
        var list = Json.getStringListOrDefault(
                obj, "strings", Collections::emptyList
        );
        Assertions.assertIterableEquals(strings, list);
    }

    @Test
    public void testListOrDefaultWithoutKey(){
        var list = Json.getStringListOrDefault(
                new JSONObject(), "strings", () -> List.of("defaultString")
        );
        Assertions.assertIterableEquals(List.of("defaultString"), list);
    }

}
